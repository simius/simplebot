using Decal.Adapter;
using System;
using System.Collections.Generic;
using SimpleBot.Data;
using System.Linq;

namespace SimpleBot
{

	public class Settings
	{ //Temporary.

		public const int MAX_REQUEST_COUNT = 3;

	}


	[FriendlyName("SimpleBot")]
	public partial class PluginCore : PluginBase
	{
		private View mainView;
		private Chat.ChatHandler chatHandler;
		private static List<Decal.Filters.Spell> spells = new List<Decal.Filters.Spell>();


		#region Helpers
		/*
		private void DumpSpells()
		{
			var spellsFull = new List<object>();
			var spells = new Dictionary<int, object>();
			FileService fs = (Decal.Filters.FileService)Core.FileService;
			for (int i = 0; i < fs.SpellTable.Length; i++)
			{
				if (fs.SpellTable[i] != null && fs.SpellTable[i].IsDebuff == false && fs.SpellTable[i].IsOffensive == false && fs.SpellTable[i].Duration == 1800.0)
				{
					if (!spells.ContainsKey(fs.SpellTable[i].Family))
					{
						var v = new
						{
							name = fs.SpellTable[i].Name,
							family = fs.SpellTable[i].Family,
							dif = fs.SpellTable[i].Difficulty
						};
						spells.Add(fs.SpellTable[i].Family, v);
					}
					spellsFull.Add(fs.SpellTable[i]);
				}
			}
			string jsonSpellsFull = Newtonsoft.Json.JsonConvert.SerializeObject(spellsFull, Newtonsoft.Json.Formatting.Indented);
			string jsonSpells = Newtonsoft.Json.JsonConvert.SerializeObject(spells, Newtonsoft.Json.Formatting.Indented);
			using (var sw = new System.IO.StreamWriter(Path + "/Spells.json"))
			{
				sw.Write(jsonSpells);
			}
			using (var sw = new System.IO.StreamWriter(Path + "/SpellsFull.json"))
			{
				sw.Write(jsonSpellsFull);
			}
		}
		*/
		#endregion

		/// <summary>
		/// Fetches a spell with the most fitting skill level from a given spell type.
		/// </summary>
		/// <param name="sbId">SimpleBot.SpellId of the spell (same as family in the decal.filters.spell).</param>
		/// <param name="selfSpell">If a self spell or other spell.</param>
		/// <returns>Spell if found, else null.</returns>
		public static Decal.Filters.Spell GetSpell(int sbId, bool selfSpell)
		{
			try
			{
				//Get all spells with given family, is not a debuff, is not offensive, is, depending on supplied boolean value, a self or not a self spell.
				var matches = spells.FindAll(s => (s.Family == sbId && (selfSpell ? (s.IsUntargetted) : (!s.IsUntargetted)) && !s.IsOffensive && !s.IsDebuff));
				//Check the diff and add 20.
				if (matches.Count == 0)
					return null;
				//Get which school it is.
				string skillName = "";
				switch (matches[0].School.Id)
				{
					case 2: //Life magic (SkillId 33).
						skillName = "Life Magic";
						break;
					case 3: //Item Enchantment (SkillId 32).
						skillName = "Item Enchantment";
						break;
					case 4: //Creature Enchantment (SkillId 31).
						skillName = "Creature Enchantment";
						break;
					default:
						return null;
				}
				var skill = CoreManager.Current.CharacterFilter.Skills.First(s => s.Name == skillName && s.Training != Decal.Adapter.Wrappers.TrainingType.Untrained && s.Training != Decal.Adapter.Wrappers.TrainingType.Unusable);
				int skillLevel = skill.Buffed + 30;
				//Find the spell in the matches list that have the highest skill level but under the one we just got.
				Decal.Filters.Spell spell = null;
				int curL = 0;
				foreach (Decal.Filters.Spell s in matches)
				{
					if (s.Difficulty > curL && s.Difficulty < skillLevel)
						spell = s;
				}
				return spell;
			}
			catch (Exception)
			{
				return null;
			}
		}

		protected override void Startup()
		{
			Debug.SetPath(Path);
			Debug.Log(eLogLevel.Debug, "Startup", "Initializing plugin.");
			try
			{
				//Load all data files.
				if (!Spell.Load(Path + "\\Resources\\Spells.json") || !Profile.Load(Path + "\\Resources\\Profiles.json"))
				{
					Core.Actions.AddChatText(string.Format("Failed to initialize {0}.An error occured. Please check the log file for more info.", AppDomain.CurrentDomain.FriendlyName), 6);
					return;
				}
				//Put all spells in a list for easier access.
				Decal.Filters.FileService fs = (Decal.Filters.FileService)CoreManager.Current.FileService;
				for (int i = 0; i < fs.SpellTable.Length; i++)
					spells.Add(fs.SpellTable[i]);
				mainView = new View(Host);
				chatHandler = new Chat.ChatHandler(this.Core);
			}
			catch (Exception ex)
			{
				Debug.Log(eLogLevel.Error, "Startup", "Failed to initialize the plugin.", ex);
			}
		}

		protected override void Shutdown()
		{
			Debug.Log(eLogLevel.Debug, "Shutdown", "Destroying plugin.");
			try
			{
				mainView.ViewDestroy();
			}
			catch (Exception ex)
			{
				Debug.Log(eLogLevel.Error, "Shutdown", "Failed to destroy the plugin.", ex);
			}
		}
	}
}