using Decal.Adapter.Wrappers;

namespace SimpleBot
{
	internal class View
	{
		#region ViewDeclarations
		private MyClasses.MetaViewWrappers.IView view;
		#endregion

		/// <summary>
		/// Once per run.
		/// Initializes the view.
		/// </summary>
		public View(PluginHost host)
		{
			view = MyClasses.MetaViewWrappers.ViewSystemSelector.CreateViewResource(host, "SimpleBot.View.view.xml");
		}

		/// <summary>
		/// Destroy on end.
		/// </summary>
		public void ViewDestroy()
		{
			view.Dispose();
		}
	}
}