﻿using System.Collections.Generic;
using SimpleBot.Data;

namespace SimpleBot.Customers
{
	/// <summary>
	/// Customer.
	/// A Customer can have multiple requests.
	/// Each request 
	/// </summary>
	public class Customer
	{
		private int guid;
		private Queue<Profile> requests;

		/// <summary>
		/// Customer GUID.
		/// </summary>
		public int GUID { get { return guid; } }

		/// <summary>
		/// Amount of requests the customer currently have active.
		/// </summary>
		public int RequestLength { get { return requests.Count; } }

		/// <summary>
		/// Customer.
		/// </summary>
		/// <param name="guid">GUID of the customer.</param>
		public Customer(int guid)
		{
			this.guid = guid;
			this.requests = new Queue<Profile>();
		}

		/// <summary>
		/// Fetches the next request and removes it from the queue.
		/// </summary>
		/// <returns>Request as a profile, or if no request is available, null.</returns>
		public Profile GetNextRequest()
		{
			if (requests.Count != 0)
				return requests.Dequeue();
			return null;
		}

		/// <summary>
		/// Add a request to the customer.
		/// </summary>
		/// <param name="request">Request to add.</param>
		public void AddRequest(Profile request)
		{
			requests.Enqueue(request);
		}

		/// <summary>
		/// Removes all requests from the customers request-queue.
		/// </summary>
		public void ClearRequests()
		{
			requests.Clear();
		}
	}
}
