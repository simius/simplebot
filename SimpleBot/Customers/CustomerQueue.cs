﻿using System.Collections.Generic;
using SimpleBot.Data;

namespace SimpleBot.Customers
{
	public class CustomerQueue
	{
		private List<Customer> customers = new List<Customer>();

		private bool AddProfile(int customerGuid, Profile profile)
		{
			int index = customers.FindIndex(c => c.GUID == customerGuid);
			if (index == -1)
			{
				customers.Add(new Customer(customerGuid));
				index = customers.Count - 1;
			}
			if (customers[index].RequestLength >= Settings.MAX_REQUEST_COUNT)
				return false;
			customers[index].AddRequest(profile);


			return true;
		}



	}
}
