﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleBot.Chat
{
	/// <summary>
	/// Chat Colors.
	/// </summary>
	enum eChatColors
	{
		GreenGameplay = 0,
		WhiteArea = 2,
		YellowTellsOutgoing = 3,
		YellowTellsIncoming = 3,
		RedInpactDamage = 6,
		BlueMagic = 7,
		YellowCoVassalAndBrodcast = 10,
		GreyEmotes = 12,
		BlueSpellWords = 17,
		OrangeAllegiance= 18,
		YellowFellow = 19,
		RedCombatIncoming = 21,
		RedCombatOutgoing = 22,
		GreenRecalls = 23,
		GreenTinker = 24,
		RedErrors = 26,
		LightBlueGeneralChat = 27,
		LightBlueTradeChat = 28,
		LightBlueLFGChat = 29,
		LightBlueRoleplayChat = 30
	};

		/// <summary>
		/// ChatMessageSender is a reference to the character that sent the ChatMessage.
		/// </summary>
	public class ChatMessageSender
	{
		private string name;
		private int guid;

		/// <summary>
		/// Name of the sender.
		/// </summary>
		public string Name { get { return name; } }
		/// <summary>
		/// Sender character GUID.
		/// </summary>
		public int GUID { get { return guid; } }

		/// <summary>
		/// Internal constructor.
		/// The ChatMessageSender will be created in the ChatMessage, which will in turn be created in the ChatHandler.
		/// </summary>
		/// <param name="name">Name of the sender.</param>
		/// <param name="guid">GUID of the sender.</param>
		internal ChatMessageSender(string name, int guid)
		{
			this.name = name;
			this.guid = guid;
		}
	}
}
