﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleBot.Chat
{
	/// <summary>
	/// ChatMessage is a object representation of a AC-Chat message.
	/// </summary>
	public class ChatMessage
	{
		protected static ChatHandler handler = null;

		private ChatMessageSender sender;
		private string message;
		private eChatType type;

		/// <summary>
		/// Sender object.
		/// </summary>
		public ChatMessageSender Sender { get { return sender; } }
		/// <summary>
		/// Message as string.
		/// </summary>
		public String Message { get { return message; } }
		/// <summary>
		/// Message type as eChatType.
		/// </summary>
		public eChatType MessageType { get { return type; } }

		/// <summary>
		/// Internal constructor.
		/// The ChatMessage is initialized in the ChatHandler and should not be possible to initialize anywhere else.
		/// </summary>
		/// <param name="raw"></param>
		internal ChatMessage(eChatType type, int sender, string senderName, string text)
		{
			this.type = type;
			this.sender = new ChatMessageSender(senderName, sender);
			this.message = text;
		}

		/// <summary>
		/// Reply to the message into the corresponding eChatType-chanel.
		/// </summary>
		/// <param name="message">Message as string.</param>
		public void Reply(string message)
		{
			if (handler == null)
				throw new Exception("Handler is null. Please initialize the handler before using it.");
			handler.SendChatMessage(type, message);
		}
	}
}
