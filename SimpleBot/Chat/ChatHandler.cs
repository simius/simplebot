﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Decal.Adapter;

namespace SimpleBot.Chat
{


	/// <summary>
	/// ChatMessage types.
	/// </summary>
	public enum eChatType 
	{
		Unknown = 999,
		// The chat type enum is mapped to the eChatColors enum.
		Local = 2,
		Tell = 3,
		Allegiance = 18,
		General = 27,
		Society = 31 //?
	};

	public class ChatHandler
	{
		#region Constants.
		const int GAME_EVENT = 0xFF7B0;
		const int CHANNELS_PRIVATE = 0x02BD;
		const int CHANNELS_GROUP = 0x0147;
		const int CHANNELS_GENERAL = 0x0295;
		
		const int CHAT_TYPE_PUBLIC = 0x02;
		const int CHAT_TYPE_PRIVATE = 0x03;
		const int CHAT_TYPE_MAGIC_RESULT = 0x07;
		const int CHAT_TYPE_PLAYER_SPELL = 0x11;
		const int CHAT_TYPE_RECALL = 0x17;

		const int CHAT_TYPE_FELLOW = 0x00000800;
		#endregion


		/// <summary>
		/// ChatMessageHandler.
		/// Delegate for incoming chat message handling.
		/// </summary>
		/// <param name="message">Message recieved.</param>
		public delegate void ChatMessageHandler(ChatMessage message);
		public static event ChatMessageHandler OnChat = null;
		private CoreManager core;

		/// <summary>
		/// ChatHandler constructor.
		/// </summary>
		/// <param name="core">CoreManage object.</param>
		public ChatHandler(CoreManager core)
		{
			this.core = core;
			OnChat += new ChatMessageHandler(OnChatInternal);
			CoreManager.Current.EchoFilter.ServerDispatch += new EventHandler<NetworkMessageEventArgs>(ServerDispatchEvent);

		}

		/// <summary>
		/// Callback on ServerDispatch.
		/// </summary>
		/// <param name="s">Sender.</param>
		/// <param name="e">Event.</param>
		void ServerDispatchEvent(object s, NetworkMessageEventArgs e)
		{
			//We are only interested in game event and specifically the events that have to do with chat.
			if (e.Message.Type == GAME_EVENT)
			{
				int ev = e.Message.Value<int>("event");
				if (ev == CHANNELS_PRIVATE) //Private channels, like /tells.
					PrivateChat(e.Message);
				else if (ev == CHANNELS_GROUP) //Group channels like /f /a.
					GroupChat(e.Message);
				else if (ev == CHANNELS_GENERAL) //General channels like /cg /ct.
					OtherChat(e.Message);
			}
		}

		/// <summary>
		/// On Chat internal.
		/// This callback listens to the OnChat event.
		/// Just used for debug logging.
		/// </summary>
		/// <param name="message">The incomming ChatMessage.</param>
		private void OnChatInternal(ChatMessage message)
		{
			Debug.Log(eLogLevel.Debug, "Chat", string.Format("Incoming chat message from {0} ({1}). Type: {2}. Message: {3}", 
								message.Sender.Name, message.Sender.GUID, message.MessageType.ToString(), message.Message));
		}

		/// <summary>
		/// On incoming chat function.
		/// Takes care of event calling and generate a chat message from passed data.
		/// </summary>
		/// <param name="type">Type of message.</param>
		/// <param name="sender">Sender GUID.</param>
		/// <param name="senderName">Sender name.</param>
		/// <param name="text">Text message as string.</param>
		private void OnIncomingChat(eChatType type, int sender, string senderName, string text)
		{
			ChatMessage message = new ChatMessage(type, sender, senderName, text);
			if (OnChat != null)
				OnChat(message);
		}


		/// <summary>
		/// Send a chat message of given type.
		/// </summary>
		/// <param name="type">Type of message.</param>
		/// <param name="message">Message as string.</param>
		/// <param name="target">Target, optional, character name.</param>
		public void SendChatMessage(eChatType type, string message, string target = "")
		{
			string msg = "/tell " + core.CharacterFilter.Name + ", " + message;
			switch (type)
			{
				case eChatType.Tell:
					if (target == "")
					{
						Debug.Log(eLogLevel.Error, "Chat", "Tried to send a tell to a character but had no target.");
						return;
					}
					msg = "/tell " + target + ", " + message;
					break;
				case eChatType.Local:
					msg = "/say " + message;
					break;
				case eChatType.Allegiance:
					msg = "/allegiance " + message;
					break;
				case eChatType.General:
					msg = "/general " + message;
					break;
				case eChatType.Society:
					msg = "/society " + message;
					break;
				default:
					Debug.Log(eLogLevel.Warning, "Chat", "Tried to send chat message to a chanel that was not set. Sending to self!");
					break;
			}
			core.Actions.InvokeChatParser(msg);
		}

		#region ServerDispatch - ChatEvents

		private void OtherChat(Message message)
		{
		}

		private void GroupChat(Message message)
		{

		}

		private void PrivateChat(Message message)
		{
			int type = message.Value<int>("type");
			string text = message.Value<string>("text");
			string name = message.Value<string>("senderName");
			int sender = message.Value<int>("sender");

			Chat.eChatType ct;
			switch (type)
			{
				case CHAT_TYPE_PUBLIC:
					ct = Chat.eChatType.Local;
					break;
				case CHAT_TYPE_PRIVATE:
					ct = Chat.eChatType.Tell;
					break;
				default:
					//Do nothing.
					return;
			}
			OnIncomingChat(ct, sender, name, text);
		}
		#endregion

	}
}
