﻿using System.IO;
using Newtonsoft.Json;

namespace SimpleBot.Data
{
	public abstract class JsonData
	{
		/// <summary>
		/// Loads given json-file and returns it as a string.
		/// </summary>
		/// <param name="path">Path to file.</param>
		/// <returns>Json data as string.</returns>
		protected static T LoadData<T>(string path)
		{
			if (!File.Exists(path))
				throw new FileNotFoundException(string.Format("Could not locate the file \"{0}\".", path));
			string jsonData = null;
			using (StreamReader reader = new StreamReader(path))
			{
				jsonData = reader.ReadToEnd();
			}
			return JsonConvert.DeserializeObject<T>(jsonData);
		}
	}
}
