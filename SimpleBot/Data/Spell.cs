﻿using System.Collections.Generic;

namespace SimpleBot.Data
{
	/// <summary>
	/// Spell data structure.
	/// </summary>
	public class Spell : JsonData
	{
		#region Static
		/// <summary>
		/// Load the data.
		/// </summary>
		/// <param name="path">Path to data file.</param>
		public static bool Load(string path)
		{
			try
			{
				Spells = LoadData<List<Spell>>(path);
				return true;
			}
			catch (System.Exception ex)
			{
				Debug.Log(eLogLevel.Error, "LoadError", string.Format("Failed to load a file ({0}).", path), ex);
				return false;
			}
		}
		/// <summary>
		/// Spell list.
		/// </summary>
		public static List<Spell> Spells { get; private set; }
		#endregion

		/// <summary>
		/// Spell ID as Int.
		/// Observe:
		/// Spell ID is not the same as the Spell id in the SpellTalble.Spell but the Family.
		/// </summary>
		public int Id { get; set; }
		/// <summary>
		/// Simple Name for the spell. Just for easier referencing in the data files.
		/// </summary>
		public string Name { get; set; }
	}
}
