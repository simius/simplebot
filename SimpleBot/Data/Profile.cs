﻿using System.Collections.Generic;

namespace SimpleBot.Data
{
	/// <summary>
	/// Profile data structure.
	/// </summary>
	public class Profile : JsonData
	{
		#region Static
		/// <summary>
		/// Load the data.
		/// </summary>
		/// <param name="path">Path to data file.</param>
		public static bool Load(string path)
		{
			try
			{
				Profiles = LoadData<List<Profile>>(path);
				return true;
			}
			catch (System.Exception ex)
			{
				Debug.Log(eLogLevel.Error, "LoadError", string.Format("Failed to load a file ({0}).", path), ex);
				return false;
			}
		}
		/// <summary>
		/// List of profiles.
		/// </summary>
		public static List<Profile> Profiles { get; private set; }
		#endregion

		/// <summary>
		/// Name of the profile.
		/// A List cause its possible to set more than one name to a profile.
		/// </summary>
		public List<string> Name { get; set; }

		/// <summary>
		/// List of spell references as Int connected to the profile.
		/// </summary>
		public List<int> Spells { get; set; }
	}
}
